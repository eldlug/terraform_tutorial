provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

resource "aws_vpc" "main_vpc" {
  cidr_block = "192.168.0.0/24"

  tags = {
    Name = "main_vpc"
  }
}

resource "aws_subnet" "srv_subnet" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "192.168.0.0/24"
  availability_zone = "us-east-1a"
}

resource "aws_network_interface" "deb_ni" {
  subnet_id   = aws_subnet.srv_subnet.id
  private_ips = ["192.168.0.22"]
  tags = {
    Name = "deb_primary_network_interface"
  }
}

resource "aws_network_interface" "rh_ni" {
  subnet_id   = aws_subnet.srv_subnet.id
  private_ips = ["192.168.0.33"]

  tags = {
    Name = "rh_primary_network_interface"
  }
}

resource "aws_internet_gateway" "gw" {
	vpc_id = aws_vpc.main_vpc.id
}

resource "aws_eip" "external" {
  instance = "${aws_instance.centosRed.id}"
  associate_with_private_ip = "192.168.0.22"
  vpc = true
  depends_on = [aws_internet_gateway.gw]
}

resource "aws_instance" "centosRed" {
  ami           = "ami-0b0af3577fe5e3532"
  instance_type = "t2.micro"
  network_interface {
	network_interface_id = aws_network_interface.rh_ni.id
	device_index = 0
	}
  tags = {
    Name = "RedHatMachine"
  }
}

resource "aws_instance" "debian10" {
  ami           = "ami-07d02ee1eeb0c996c"
  instance_type = "t2.micro"
  network_interface {
	network_interface_id = aws_network_interface.deb_ni.id
        device_index = 0
  }
  tags = {
    Name = "DebianMachine"
  }
}

